#!/usr/bin/python3
# -*- coding: utf8 -*-

from polyglot.text import Text
from svmalign.src.train_data import open_document
from nltk.tokenize.moses import MosesDetokenizer


def file_to_sents(filepath, language='en'):
    """
    :param text:
    :param language:
    :return: list of Sentences
    """

    with open(filepath, 'r') as textfile:
        text = textfile.read()
    polytext = Text(text, hint_language_code=language)

    return [sent.words for sent in polytext.sentences]


def poly_tokenize(sentence, language):
    try:
        text = Text(sentence, hint_language_code=language)
        return text.words
    except:
        return None


def parallel_file_to_words(filepath_a, filepath_b, language_a, language_b):
    text_words_a = []
    text_words_b = []

    text_a = open_document(filepath_a)
    text_b = open_document(filepath_b)

    for sent_a, sent_b in zip(text_a, text_b):
        words_a = poly_tokenize(sent_a, language_a)
        words_b = poly_tokenize(sent_b, language_b)

        if words_a and words_b:
            text_words_a.append(words_a)
            text_words_b.append(words_b)

    return text_words_a, text_words_b


def word_to_sent(text):
    return MosesDetokenizer().detokenize(text, return_str=True)