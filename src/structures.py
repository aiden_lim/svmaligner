#!/usr/bin/python3
# -*- coding: utf-8 -*-


class SentencePair(list):
    """
    An association of two sentences with one attribute
    to indicate if they are considered aligned.
    """
    def __init__(self, sentence_a, sentence_b, aligned=None):
        super(SentencePair, self).__init__([sentence_a, sentence_b])
        self.a = sentence_a
        self.b = sentence_b
        self.aligned = aligned
