#!/usr/bin/python3
# -*- coding: utf-8 -*-

from svmalign.src.svm_model import AlignModel
from svmalign.src.scoring import WordPairScore, SentencePairScore
from svmalign.src.train_data import training_alignments_from_documents, training_scrambling_from_documents
from svmalign.src.sequence import SequenceAligner
from svmalign.src.tokenizer import parallel_file_to_words

OPTIMIZE_SAMPLE_SET_SIZE = 100


def model_train(corpus_a, corpus_b, word_scores_filepath, lang_a='en', lang_b='ja'):
    """
    Creates and trains a `AlignModel` with the basic configuration and
    default values.

    `corpus_a/b` is the path to a parallel corpus used for training,

    `word_scores_filepath` is the path to a csv file (possibly gzipped) with
    word dictionary data. (for ex. "house,casa,0.91").

    """
    # Word score
    word_pair_score = WordPairScore(word_scores_filepath)
    A, B = parallel_file_to_words(corpus_a, corpus_b, lang_a, lang_b)
    alignments = training_alignments_from_documents(A, B)

    sentence_pair_score = SentencePairScore()
    sentence_pair_score.train(alignments, word_pair_score)
    # Align model
    metadata = {"lang_a": lang_a, "lang_b": lang_b}
    gap_penalty = 0.49
    threshold = 1.0
    document_aligner = SequenceAligner(sentence_pair_score, gap_penalty)
    model = AlignModel(document_aligner, threshold, metadata=metadata)
    A, B, correct = training_scrambling_from_documents(A[:OPTIMIZE_SAMPLE_SET_SIZE], B[:OPTIMIZE_SAMPLE_SET_SIZE])
    model.optimize_gap_penalty_and_threshold(A, B, correct)

    return model
