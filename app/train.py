#!/usr/bin/env python3
# coding: utf-8

"""
Trains a model.

Usage:
    train [options] <doc_a> <doc_b> <dictionary> <model_folder>

Options:
  -a --lang-a=<language>      The language of the document A [default: en]
  -b --lang-b=<language>      The language of the document B [default: ja]
"""

import os
from docopt import docopt
from svmalign.src.training import model_train


if __name__ == "__main__":
    args = docopt(__doc__)
    lang_a = args["--lang-a"]
    lang_b = args["--lang-b"]
    corpus_a = args["<doc_a>"]
    corpus_b = args["<doc_b>"]
    dictionary = args["<dictionary>"]

    output_folder = args["<model_folder>"]
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    model = model_train(corpus_a, corpus_b, dictionary, lang_a, lang_b)
    model.save(output_folder)
