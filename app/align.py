#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Aligns two documents.

Inputs:
    model_folder: The directory where a trained model is kept.
    document_a, document_b: The files or urls for the alignment.

Output:
    The output is written to stdout. View the -f option for supported output formats.

Usage:
    align [options] <model_folder> <document_a> <document_b>

Options:
  -a --lang-a=<language>                The language of the document A [default: en]
  -b --lang-b=<language>                The language of the document B [default: es]
  -o --output=<output>    The output format options are plaintext and tmx [default: plaintext]
                                        The plaintext output consists of alternating sentences in the target
                                        languages.
  -h --help                             Show this screen.
"""

import os
from docopt import docopt
from svmalign.src.svm_model import AlignModel
from svmalign.src.tokenizer import file_to_sents, word_to_sent


def read_corpusfile(filename, language):
    with open(filename, 'r') as f:
        text_data = f.read()

        return file_to_sents(text_data, language)


def write_plaintext(stream, pairs):
    for a, b in pairs:
        stream.write(a.to_text())
        stream.write('\n')
        stream.write(b.to_text())
        stream.write('\n')


if __name__ == "__main__":
    from sys import stdout

    args = docopt(__doc__)
    output_location = args['--output']
    lang_a = args['--lang-a']
    lang_b = args['--lang-b']
    model_path = os.path.abspath(args['<model_folder>'])

    document_a = file_to_sents(args['<document_a>'], lang_a)
    document_b = file_to_sents(args['<document_b>'], lang_b)
    model = AlignModel.load(model_path)
    print('Model Loaded')
    pairs = model.align(document_a, document_b)

    print("Output")
    for pair in pairs:
        print(word_to_sent(pair[0]), word_to_sent(pair[1]))
    if output_location:
        with open(output_location, 'w') as outfile:
            for pair in pairs:
                outfile.write('{0}|||{1}\n'.format(pair[0], pair[1]))

    # else:
        # write_plaintext(stdout, pairs)